const webdriver = require('selenium-webdriver');
const USERNAME = "kim.eliasdiaz";
const KEY = 'pd7dyT83kUOtYsQaNLCiD3kUbzAk714UFLAFX3btqIPhcbJlek';
const GRID_HOST = 'hub.lambdatest.com/wd/hub';
const gridUrl = 'https://' + USERNAME + ':' + KEY + '@' + GRID_HOST;
const { Builder, By, Key, until } = require('selenium-webdriver')
const assert = require('assert')
 
function Test() {
    const capabilities = {
        platform: 'Windows 10',
        browserName: 'Chrome',
        version: '90.0',
        resolution: '1024x768',
        network: true,
        visual: true,
        console: true,
        video: true,
        name: 'Test Remoto', // name of the test
        build: 'Selenium' // name of the build
    }

        describe('Test Pruebas Funcionales', function() {
            this.timeout(30000000)
            const driver = new webdriver.Builder()
            .usingServer(gridUrl)
            .withCapabilities(capabilities)
            .build();
           
          it('Pruebas Funcionales - Register', async function() {
            await driver.get("http://35.184.31.96/")
            await driver.manage().window().setRect({ width: 1589, height: 1032 })
            await driver.findElement(By.css(".login__create-container__form-container__form--name")).click()
            await driver.findElement(By.css(".login__create-container__form-container__form--name")).sendKeys("Kimberly Elias")
            await driver.findElement(By.css(".login__create-container__form-container__form--email")).click()
            await driver.findElement(By.css(".login__create-container__form-container__form--email")).sendKeys("kim.eliasdiaz@gmail.com")
            await driver.findElement(By.css(".login__create-container__form-container__form--password")).click()
            await driver.findElement(By.css(".login__create-container__form-container__form--password")).sendKeys("201700507")
            await driver.findElement(By.css(".login__create-container__form-container__form--submit")).click()
          })
          
          it('Pruebas Funcionales - Login', async function() {
            await driver.get("http://35.184.31.96/")
            await driver.manage().window().setRect({ width: 1589, height: 1032 })
            await driver.findElement(By.css(".login__welcome-back__main-container > .login__welcome-back__main-container__button-container")).click()
            await driver.findElement(By.css(".login__login-container__main-container__form-container__form--email")).click()
            await driver.findElement(By.css(".login__login-container__main-container__form-container__form--email")).sendKeys("kim.eliasdiaz@gmail.com")
            await driver.findElement(By.css(".login__login-container__main-container__form-container__form--password")).click()
            await driver.findElement(By.css(".login__login-container__main-container__form-container__form--password")).sendKeys("201700507")
            await driver.findElement(By.css(".login__login-container__main-container__form-container__form--submit")).click()
          })
         
          
          })
 
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

Test();
